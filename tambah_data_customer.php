<html>
<header>
  <?php
  
  session_start();
  if (!isset($_SESSION['username']) || $_SESSION['jabatan'] !== "humas"){
  header("Location:./login.php");
}
?>
<?php include ('header.php');?>

</header>
<body>

  <div class = "wrapper">
    <!-- navbar -->
    <?php include ("navbar.php"); ?>
    <!-- SideBar -->
    <?php include ("sidebar_humas.php"); ?> 
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">

        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-12">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Tambah Data Customer</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="proses_data_customer.php" method = "post" name="formbarang">
                  <div class="card-body">


                    <div class="form-group">
                      <label for="id">ID Customer</label>
                      <input type="text" class="form-control" id="id" name="id">
                    </div>
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input type="text" class="form-control" id="nama" name="nama" >
                    </div>
                    <div class="form-group">
                      <label for="alamat">Alamat</label>
                      <input type="text" class="form-control"  id="alamat" name="alamat">
                    </div>
                    <div class="form-group">
                      <label for="email">email</label>
                      <input type="email" class="form-control" id="email" name="email">
                    </div>
                    <div class="form-group">
                      <label for="telepon">telepon</label>
                      <input type="tel" class="form-control" id="telepon" name="telepon">
                    </div>
                    
                  </div>
                  <div class="card-footer">
                    <button type="submit" id= "submit" name = "submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>


                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </section>
          </div>
        </div>
      </div>
    </div>
    <script>
      function customer(){
        var id = $("#id_customer").val()
        $.get( "proses_transaksi_accounting.php?id="+id, function( data ) {
          $.each(JSON.parse(data), function(key, value) {
          // alert(value);
          $('#nama').val(value);
          
        });


        });
      }
    </script>
    <?php include ('footer.php');?>

  </body>


  </html>
</body>
</html>