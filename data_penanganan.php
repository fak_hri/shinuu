<html>
<header>
    <?php
  // echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx".$_SESSION['username'];
session_start();
if (!isset($_SESSION['username']) || $_SESSION['jabatan'] !== "humas"){
header("Location:./login.php");
}
?>
    <?php include ('header.php');?>
</header>
<body>

    <div class = "wrapper">
        <!-- navbar -->
        <?php include ("navbar.php"); ?>
        <!-- SideBar -->
        <?php include ("sidebar_humas.php"); ?> 
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <div class="container-fluid">
                <div class="row mb-2">

                  <div class="col-sm-6">

                  </div>
              </div>
          </div><!-- /.container-fluid -->
      </section>
      <!-- Main content -->
      <section class="content">
       
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">

                        <h3 class="card-title">Data Penanganan</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Id Keluhan</th>
                                    <th>Nama Customer</th>
                                    <th>Kategori Keluhan</th>
                                    <th>Penanganan</th>
                                    <th>Prioritas</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                    
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                include ('koneksi.php');
                                $i = 1;
                                $sql="SELECT *, k.id id_keluhan FROM penanganan p inner join keluhan k on (p.id_keluhan = k.id) inner join customer c on (p.id_customer = c.id)";
                                $result=mysqli_query($conn,$sql);

                                // Associative array
                                while($data = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                    if ($data['status'] == 0){
                                        $status = "Belum Ditangani";
                                    }else if ($data['status'] == 1){
                                        $status = "Sedang Ditangani";
                                    }else if ($data['status'] == 2){
                                        $status = "Sudah Ditangani";
                                    } 
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $data["id_keluhan"]; ?></td>
                                    <td><?php echo $data["nama"]; ?></td>
                                    <td><?php echo $data["kategori"]; ?></td>
                                    <td><?php echo $data["penanganan"]; ?></td>
                                    <td><?php echo $data["prioritas"]; ?></td>
                                    <td><?php echo $status ?></td>
                                    
                                    

                                    <td><a class="btn btn-info btn-sm" href="aksi_data_penanganan.php?ni=<?php echo $data['id'];?>">
                                        
                                    Detail</a>
                                    
                                </td>               

                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
</div>
</div>
</div>
</div>
<?php include ('footer.php');?>
</body>


</html>