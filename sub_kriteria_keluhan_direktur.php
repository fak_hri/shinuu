<html>
<header>
  <?php
  // echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx".$_SESSION['username'];
  session_start();
  if (!isset($_SESSION['username']) || $_SESSION['jabatan'] !== "direktur"){
    header("Location:./login.php");
  }
  ?>
  <?php include ('header.php');?>
</header>
<body>

  <div class = "wrapper">
    <!-- navbar -->
    <?php include ("navbar.php"); ?>
    <!-- SideBar -->
    <?php include ("sidebar_direktur.php"); ?> 
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">

            <div class="col-sm-6">

            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
      <!-- Main content -->
      <section class="content">
       <a class="btn btn-info" href="tambah_data_kriteria_keluhan_direktur.php">

       Tambah</a><br><br>
       <div class="row">
        <div class="col-12">
          <div class="card card-primary">
            <div class="card-header">

              <h3 class="card-title">Data Kriteria Keluhan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Id Sub-Kriteria</th>
                    <th>Nama Sub-Kriteria</th>

                    <th>Aksi</th>


                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(isset($_GET['ni'])) {
                    $id = $_GET['ni'];
                    include ('koneksi.php');
                    $i = 1;
                    $sql="SELECT * from sub_kriteria_keluhan where id_kriteria_keluhan = '".$id."'";
                    $result=mysqli_query($conn,$sql);

                                // Associative array
                    while($data = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                  //  if ($data['status'] == 0){
                                  //     $status = "Belum Ditangani";
                                  // }else if ($data['status'] == 1){
                                  //     $status = "Sedang Ditangani";
                                  // }else if ($data['status'] == 2){
                                  //     $status = "Sudah Ditangani";
                                  // } 
                      ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $data["id"]; ?></td>
                        <td><?php echo $data["nama"]; ?></td>





                        <td>
                        <a class="btn btn-info btn-sm" href="edit_data_kriteria_keluhan_direktur.php?ni=<?php echo $data['id'];?>">

                        Edit</a>
                        <a class="btn btn-info btn-sm" href="delete_data_kriteria_keluhan_direktur.php?ni=<?php echo $data['id'];?>">

                        Delete</a>

                      </td>               

                    </tr>
                    <?php
                    $i++;
                  }
                }
                ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </section>
    </div>
  </div>
</div>
</div>
<?php include ('footer.php');?>
</body>


</html>