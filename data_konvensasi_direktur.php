<html>
<header>
    <?php
    // echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx".$_SESSION['username'];
    session_start();
    if (!isset($_SESSION['username']) || $_SESSION['jabatan'] !== "direktur"){
        header("Location:./login.php");
    }
    ?>
    <?php include ('header.php');?>
</header>
<body>

    <div class = "wrapper">
        <!-- navbar -->
        <?php include ("navbar.php"); ?>
        <!-- SideBar -->
        <?php include ("sidebar_direktur.php"); ?> 
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <div class="container-fluid">
                <div class="row mb-2">

                  <div class="col-sm-6">

                  </div>
              </div>
          </div><!-- /.container-fluid -->
      </section>
      <!-- Main content -->
      <section class="content">
       <form action="data_konvensasi_direktur.php" method = "post" name="konvensasi">

        <div class="form-group col-md-4">
          <label for="periode">Periode</label>
          <select class="form-control" name="periode" id="periode">
            <option value="Januari - Maret">Januari - Maret</option>
            <option value="April - Juni">April - Juni</option>
            <option value="Juli - Oktober">Juli - Oktober</option>
            <option value="November - Desember">November - Desember</option>

        </select>
    </div>

    <button type="submit" id= "submit" name = "submit" class="btn btn-primary">Submit</button>

</form>
<div class="row">
    <div class="col-12">
        <div class="card card-primary">
            <div class="card-header">

                <h3 class="card-title">Data Konvensasi</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Customer</th>
                            <th>Nama Customer</th>
                            <th>Periode</th>



                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!isset($_POST['submit'])) {
                            include ('koneksi.php');
                            $i = 1;
                            $sql="SELECT * FROM konvensasi k inner join customer c on (k.id_customer = c.id) ";
                            $result=mysqli_query($conn,$sql);

                            // Associative array
                            while($data = mysqli_fetch_array($result,MYSQLI_ASSOC)){

                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $data["id_customer"]; ?></td>
                                    <td><?php echo $data["nama"]; ?></td> 
                                    <td><?php echo  $data["periode"]; ?></td>









                                </tr>
                                <?php
                                $i++;
                            }
                        } else {
                            $periode = $_POST['periode'];
                            include ('koneksi.php');
                            $i = 1;
                            $sql="SELECT * FROM konvensasi k inner join customer c on (k.id_customer = c.id) where periode = '".$periode."'";
                            $result=mysqli_query($conn,$sql);

                            // Associative array
                            while($data = mysqli_fetch_array($result,MYSQLI_ASSOC)){

                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $data["id_customer"]; ?></td>
                                    <td><?php echo $data["nama"]; ?></td> 
                                    <td><?php echo  $data["periode"]; ?></td>









                                </tr>
                                <?php
                                $i++;
                            }
                        }   
                            ?>
                        
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </section>
    </div>
</div>
</div>
</div>
<?php include ('footer.php');?>
</body>


</html>