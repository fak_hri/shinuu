<!-- Main Sidebar Container -->

<aside class="main-sidebar sidebar-dark-primary elevation-4" style="background-color: #c73e38">
 <!-- Sidebar -->
 <div class="sidebar">
  <!-- Sidebar user panel (optional) -->
  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
    <div class="image">
      <img src="assets/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
    </div>
    <div class="info">
      <a href="#" class="d-block">Alexander Pierce</a>
    </div>
  </div>

  <!-- Sidebar Menu -->
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
           <li class="nav-item has-treeview">
            <a href="index_direktur.php" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                DASHBOARD
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="data_keluhan_direktur.php" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Data Keluhan
                
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="data_konvensasi_direktur.php" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Data Konvensasi
                
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="data_prioritas_keluhan.php" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Data Prioritas Keluhan
                
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="data_kriteria_keluhan.php" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Data Kriteria Keluhan
                
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="data_kriteria_konvensasi.php" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Data Kriteria Konvensasi
                
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="data_prioritas_keluhan.php" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Prioritas Keluhan
                
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="penentuan_konvensasi.php" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Penentuan Konvensasi
                
              </p>
            </a>
          </li>
          
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>