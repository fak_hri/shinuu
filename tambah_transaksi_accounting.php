<html>
<header>
  <?php
  
  session_start();
  if (!isset($_SESSION['username']) || $_SESSION['jabatan'] !== "accounting"){
  header("Location:./login.php");
}
?>
<?php include ('header.php');?>

</header>
<body>

  <div class = "wrapper">
    <!-- navbar -->
    <?php include ("navbar.php"); ?>
    <!-- SideBar -->
    <?php include ("sidebar_accounting.php"); ?> 
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">

        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-12">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Tambah Transaksi</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="proses_transaksi_accounting.php" method = "post" name="formbarang">
                  <div class="card-body">


                    <div class="form-group">
                      <label for="id">ID Transaksi</label>
                      <input type="text" class="form-control" id="id" name="id">
                    </div>
                    <div class="form-group">
                      <label for="id_customer">ID Customer</label>
                      <input type="text" class="form-control" id="id_customer" name="id_customer" onchange="customer()">
                    </div>
                    <div class="form-group">
                      <label for="nama">Nama Customer</label>
                      <input type="text" class="form-control" readonly id="nama" name="nama">
                    </div>
                    <div class="form-group">
                      <label for="tanggal">Tanggal Transaksi</label>
                      <input type="date" class="form-control" id="tanggal" name="tanggal">
                    </div>
                    <div class="form-group">
                      <label for="tujuan">Tujuan</label>
                      <input type="text" class="form-control" id="tujuan" name="tujuan">
                    </div>
                    <div class="form-group">
                      <label for="kelas">Kelas Transaksi</label>
                      <select class="form-control" name="kelas" id="kelas">
                        <option value="A">A-Tronton</option>
                        <option value="B">B-Engkel Bak</option>
                        <option value="C">C-Wing Box</option>
                        <option value="D">D-Engkel Box</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="qty">Banyak Kendaraan</label>
                      <input type="text" class="form-control" id="qty" name="qty">
                    </div>
                  </div>
                  <div class="card-footer">
                    <button type="submit" id= "submit" name = "submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>


                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </section>
          </div>
        </div>
      </div>
    </div>
    <script>
      function customer(){
        var id = $("#id_customer").val()
        $.get( "proses_transaksi_accounting.php?id="+id, function( data ) {
          $.each(JSON.parse(data), function(key, value) {
          // alert(value);
          $('#nama').val(value);
          
        });


        });
      }
    </script>
    <?php include ('footer.php');?>

  </body>


  </html>
</body>
</html>