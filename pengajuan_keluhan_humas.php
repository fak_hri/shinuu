<html>
<header>
    <?php
    // echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx".$_SESSION['username'];
    session_start();
    if (!isset($_SESSION['username']) || $_SESSION['jabatan'] !== "humas"){
    header("Location:./login.php");
}
?>
<?php include ('header.php');?>
</header>
<body>

    <div class = "wrapper">
        <!-- navbar -->
        <?php include ("navbar.php"); ?>
        <!-- SideBar -->
        <?php include ("sidebar_humas.php"); ?> 
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <div class="container-fluid">
                <div class="row mb-2">

                  <div class="col-sm-6">

                  </div>
              </div>
          </div><!-- /.container-fluid -->
      </section>
      <!-- Main content -->
      <section class="content">

        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">

                        <h3 class="card-title">Pengajuan Keluhan</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Id Transaksi</th>
                                    <th>Nama Customer</th>
                                    <th>Tanggal</th>
                                    <th>Tujuan</th>
                                    <th>Kelas Transaksi</th>
                                    <th>Banyak Kendaraan</th>
                                    <th>Aksi</th>
                                    

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                include ('koneksi.php');
                                $i = 1;
                                $sql="SELECT *,t.id id_transaksi FROM transaksi t inner join customer c on (t.id_customer = c.id) ";
                                $result=mysqli_query($conn,$sql);

                                // Associative array
                                while($data = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $data["id_transaksi"]; ?></td>
                                    <td><?php echo $data["nama"]; ?></td>
                                    <td><?php echo $data["tanggal"]; ?></td>
                                    <td><?php echo $data["tujuan"]; ?></td>
                                    <?php if($data["kelas"] == "A"){
                                        ?><td>A-Tronton</td><?php
                                    }elseif($data["kelas"] == "B"){
                                        ?><td>B-Engkel Bak</td><?php
                                    }elseif($data["kelas"] == "C"){
                                        ?><td>C-Wing Box</td><?php
                                    }elseif($data["kelas"] == "D"){
                                        ?><td>D-Engkel Box</td><?php
                                    }?>
                                    
                                    <td><?php echo $data["qty"]; ?></td>


                                    <td><a class="btn btn-info btn-sm" href="tambah_keluhan_humas.php?ni=<?php echo $data['id_transaksi'];?>">
                                       
                                    Tambah Keluhan</a>
                                    
                                </td>               

                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
</div>
</div>
</div>
</div>
<?php include ('footer.php');?>
</body>


</html>