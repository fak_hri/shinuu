<html>
<header>
  <?php
  
  session_start();
  if (!isset($_SESSION['username']) || $_SESSION['jabatan'] !== "humas"){
    header("Location:./login.php");
  }


  ?>
  <?php include ('header.php');?>

</header>
<body>
  <?php 
  if(isset($_GET['ni'])){
    $ni   = $_GET['ni'];
    include('koneksi.php');
    $query  = mysqli_query($conn,'SELECT *, k.id id_keluhan FROM penanganan p inner join keluhan k on (p.id_keluhan = k.id) inner join customer c on (p.id_customer = c.id) where p.id = "'.$ni.'"');
  // $result=mysqli_query($conn,$query);
    $data = mysqli_fetch_array($query);
  // echo $data['id_transaksi'];

    if ($data['status'] == 0){
      $status = "Belum Ditangani";
    }else if ($data['status'] == 1){
      $status = "Sedang Ditangani";
    }else if ($data['status'] == 2){
      $status = "Sudah Ditangani";
    } 
  }
  ?>

  <div class = "wrapper">
    <!-- navbar -->
    <?php include ("navbar.php"); ?>
    <!-- SideBar -->
    <?php include ("sidebar_humas.php"); ?> 
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">

        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-12">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Tambah Keluhan</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="proses_data_penanganan.php" method = "post" name="penaganan">
                  <div class="card-body">

                    <div class="form-group">
                      <label for="id">ID Keluhan</label>
                      <input type="hidden" class="form-control" id="id" name="id"  value ="<?php echo $data['id'];?>">
                      <input type="text" class="form-control" id="id_keluhan" name="id_keluhan" readonly value ="<?php echo $data['id_keluhan'];?>">
                    </div>
                    <div class="form-group">
                      <label for="nama">Nama Customer</label>
                      <input type="text" class="form-control" id="nama" name="nama" readonly value="<?php echo $data['nama'];?>">
                    </div>
                    <div class="form-group">
                      <label for="kategori">Kategori Keluhan</label>
                      <input type="text" class="form-control" readonly id="kategori" name="kategori" readonly value="<?php echo $data['kategori'];?>">
                    </div>
                    <div class="form-group">
                      <label for="penanganan">Penanganan</label>
                      <input type="text" class="form-control" readonly id="penanganan" name="penanganan" readonly value="<?php echo $data['penanganan'];?>">
                    </div>
                 
                  <div class="form-group">
                    <label for="prioritas">Prioritas</label>
                    <input type="text" class="form-control" readonly id="prioritas" name="prioritas" readonly value="<?php echo $data['prioritas'];?>">
                  </div>

                  <div class="form-group">
                    <label for="status">Status</label>
                    <select class="form-control" name="status" id="status">
                      <option value="<?php echo $data['status']?>"><?php echo $status ?></option>
                      <option value="0">Belum Ditangani</option>
                      <option value="1">Sedang Ditangani</option>
                      <option value="2">Sudah Ditangani</option>
                    </select>
                  </div>
                  
                </div>
                <div class="card-footer">
                  <button type="submit" id= "submit" name = "submit" class="btn btn-primary">Submit</button>
                </div>
              </form>


              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section>
        </div>
      </div>
    </div>
  </div>

  <?php include ('footer.php');?>

</body>


</html>
</body>
</html>