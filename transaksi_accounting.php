<html>
<header>
    <?php
  // echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx".$_SESSION['username'];
session_start();
if (!isset($_SESSION['username']) || $_SESSION['jabatan'] !== "accounting"){
header("Location:./login.php");
}
?>
    <?php include ('header.php');?>
</header>
<body>

    <div class = "wrapper">
        <!-- navbar -->
        <?php include ("navbar.php"); ?>
        <!-- SideBar -->
        <?php include ("sidebar_accounting.php"); ?> 
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <div class="container-fluid">
                <div class="row mb-2">

                  <div class="col-sm-6">

                  </div>
              </div>
          </div><!-- /.container-fluid -->
      </section>
      <!-- Main content -->
      <section class="content">
        <a href="tambah_transaksi_accounting.php"  class="btn btn-primary">TAMBAH</a> <br><br>
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">

                        <h3 class="card-title">Transaksi</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Id Transaksi</th>
                                    <th>Nama Customer</th>
                                    <th>Tanggal</th>
                                    <th>Tujuan</th>
                                    <th>Kelas Transaksi</th>
                                    <th>Banyak Kendaraan</th>
                                    
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                include ('koneksi.php');
                                $i = 1;
                                $sql="SELECT *,t.id id_transaksi FROM transaksi t inner join customer c on (t.id_customer = c.id)";
                                $result=mysqli_query($conn,$sql);

                                // Associative array
                                while($data = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $data["id_transaksi"]; ?></td>
                                    <td><?php echo $data["nama"]; ?></td>
                                    <td><?php echo $data["tanggal"]; ?></td>
                                    <td><?php echo $data["tujuan"]; ?></td>
                                    <td><?php echo $data["kelas"]; ?></td>
                                    <td><?php echo $data["qty"]; ?></td>
                                    

                                    <!-- <td><a class="btn btn-info btn-sm" href="edit_barang_masuk.php?ni=<?php //echo $data['kode_barang'];?>">
                                        <i class="fas fa-pencil-alt"></i>
                                    Edit</a>
                                    <a class="btn btn-danger btn-sm" href="deletebarang_masuk.php?ni=<?php //echo $data['kode_barang'];?>">
                                        <i class="fas fa-trash">
                                        </i>
                                    Delete</a>
                                </td>       -->         

                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
</div>
</div>
</div>
</div>
<?php include ('footer.php');?>
</body>


</html>