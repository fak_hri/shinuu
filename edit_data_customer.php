<html>
<header>
  <?php
  
  session_start();
  if (!isset($_SESSION['username']) || $_SESSION['jabatan'] !== "humas"){
    header("Location:./login.php");
  }


  ?>
  <?php include ('header.php');?>

</header>
<body>
  <div class = "wrapper">
    <!-- navbar -->
    <?php include ("navbar.php"); ?>
    <!-- SideBar -->
    <?php include ("sidebar_humas.php"); ?> 
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          
        
      </section>
      <?php
      include('koneksi.php');
      if(isset($_GET['ni'])){
        $ni		= $_GET['ni'];
        $query	= mysqli_query($conn,'select * from customer  where id = "'.$ni.'"');
        $data  	= mysqli_fetch_array($query);
       

      }

     

      include "koneksi.php";
     

      $query = "SELECT * FROM warna ORDER BY kode_warna ASC";

     


      ?>
      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Data Customer</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="proses_data_customer.php" method = "post" name="formbarang">
                <div class="card-body">
                  <div class="form-group">
                    <label for="id">Id</label>
                    <?php
                    echo '<input type="text" class="form-control" id="id" name="id" value="'.$data['id'].'">'
                    ?>
                  </div>
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <?php
                    echo '<input type="text" class="form-control" id="nama" name="nama" value="'.$data['nama'].'">'
                    ?>
                  </div> 
                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <?php
                    echo '<input type="text" class="form-control" id="alamat" name="alamat" value="'.$data['alamat'].'" >'
                    ?>
                  </div>
                  <div class="form-group">
                    <label for="email">Email</label>
                    <?php
                    echo '<input type="email" class="form-control" id="email" name="email" value="'.$data['email'].'">'
                    ?>
                  </div>  
                  <div class="form-group">
                    <label for="telepon">Telepon</label>
                    <input type="tel" class="form-control" id="telepon" value="<?php echo $data['telepo']?>" name="telepon"  >
                  </div>
                  

                </div> 
              </div>
              <div class="card-footer">
                <button type="submit" id= "submit_edit" name = "submit_edit" class="btn btn-primary">Submit</button>
              </div>
            </form>


            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </section>
      </div>
    </div>
  </div>
</div>
<script>
 function totals(){
  var harga = $("#harga").val()
  var jumlah = $("#jumlah").val()
  var total = harga * jumlah
  $("#total").val(total)
}
</script>
<?php include ('footer.php');?>
</body>


</html>
</body>
</html>