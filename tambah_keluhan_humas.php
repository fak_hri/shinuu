<html>
<header>
  <?php
  
  session_start();
  if (!isset($_SESSION['username']) || $_SESSION['jabatan'] !== "humas"){
    header("Location:./login.php");
  }


  ?>
  <?php include ('header.php');?>

</header>
<body>
  <?php 
  if(isset($_GET['ni'])){
    $ni   = $_GET['ni'];
    include('koneksi.php');
    $query  = mysqli_query($conn,'SELECT *,t.id id_transaksi FROM transaksi t inner join customer c on (t.id_customer = c.id) where t.id = "'.$ni.'"');
     // $result=mysqli_query($conn,$query);
    $data = mysqli_fetch_array($query);
      // echo $data['id_transaksi'];


  }
  ?>

  <div class = "wrapper">
    <!-- navbar -->
    <?php include ("navbar.php"); ?>
    <!-- SideBar -->
    <?php include ("sidebar_humas.php"); ?> 
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">

        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-12">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Tambah Keluhan</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="proses_keluhan_humas.php" method = "post" name="formbarang">
                  <div class="card-body">

                    <div class="form-group">
                      <label for="id">ID Keluhan</label>
                      <input type="text" class="form-control" id="id" name="id">
                    </div>
                    <div class="form-group">
                      <label for="id_transaksi">ID Transaksi</label>
                      <input type="text" class="form-control" id="id_transaksi" name="id_transaksi" readonly value="<?php echo $data['id_transaksi'];?>">
                    </div>
                    <div class="form-group">
                      <label for="nama">Nama Customer</label>
                      <input type="text" class="form-control" readonly id="nama" name="nama" readonly value="<?php echo $data['nama'];?>">
                      <input type="hidden" class="form-control" readonly id="id_customer" name="id_customer" readonly value="<?php echo $data['id_customer'];?>">
                    </div>
                    <div class="form-group">
                      <label for="tanggal">Tanggal Keluhan</label>
                      <input type="date" class="form-control" id="tanggal" name="tanggal">
                    </div>
                    <div class="form-group">
                      <label for="kategori">Kategori Keluhan</label>
                      <select class="form-control" name="kategori" id="kategori">
                        <option value="Kerusakan Barang">Kerusakan Barang</option>
                        <option value="Jumlah Barang">Jumlah Barang</option>
                        <option value="Keterlambatan">Keterlambatan</option>
                        <option value="Lamanya Pemuatan">Lamanya Pemuatan</option>
                        <option value="Kesalahpahaman">Kesalahpahaman</option>
                        <option value="Kondisi Kendaraan atau Supir">Kondisi Kendaraan atau Supir</option>
                         <option value="Administrasi">Administrasi</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="keluhan">Isi Keluhan</label>
                      <textarea class="form-control" id="keluhan" name="keluhan"></textarea>
                    </div>
                    <div class="form-group">
                      <label for="tujuan">Tujuan</label>
                      <input type="text" class="form-control" id="tujuan" name="tujuan" readonly value="<?php echo $data['tujuan'];?>">
                    </div>
                     <input type="hidden" class="form-control" id="status" name="status" readonly value="0">
                    <div class="form-group">
                      <label for="tujuan">Kelas</label>
                      <?php 
                      if($data["kelas"] == "A"){
                        $kelas = "A-Tronton";
                      }elseif($data["kelas"] == "B"){
                        $kelas = "B-Engkel Bak";

                      }elseif($data["kelas"] == "C"){
                        $kelas = "C-Wing Box";

                      }elseif($data["kelas"] == "D"){
                        $kelas = "D-Engkel Box";

                      }?>
                      <input type="text" class="form-control" id="kelas" name="kelas" readonly value="<?php echo $kelas;?>">
                    </div>
                    <div class="form-group">
                      <label for="qty">Banyak Kendaraan</label>
                      <input type="text" class="form-control" id="qty" name="qty" readonly value="<?php echo $data['qty'];?>">
                    </div>
                  </div>
                  <div class="card-footer">
                    <button type="submit" id= "submit" name = "submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>


                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </section>
          </div>
        </div>
      </div>
    </div>

    <?php include ('footer.php');?>

  </body>


  </html>
</body>
</html>