-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2019 at 03:03 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shinu`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telepo` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `nama`, `alamat`, `email`, `telepo`) VALUES
(1, 'abdul', 'dago', 'abdul@m.c', '08937377267'),
(2, 'asep', 'caheum', 'asep@c.c', '089636276373'),
(3, 'Jack ripper', 'Sekeloa', 'jak@j.c', '0894352435243');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id` int(11) NOT NULL,
  `kelas` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id`, `kelas`) VALUES
(1, 'tronton'),
(2, 'truck');

-- --------------------------------------------------------

--
-- Table structure for table `keluhan`
--

CREATE TABLE `keluhan` (
  `id` int(11) NOT NULL,
  `id_transaksi` int(11) DEFAULT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `kategori` varchar(50) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keluhan` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `penanganan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keluhan`
--

INSERT INTO `keluhan` (`id`, `id_transaksi`, `id_customer`, `kategori`, `tanggal`, `keluhan`, `status`, `penanganan`) VALUES
(0, 2, 1, 'Kerusakan Barang', '2019-11-13', 'Pada Rusak pak', 0, NULL),
(1, 1, 1, 'Kerusakan Barang', '2019-11-11', 'Mobil Tabrakan', 1, 'Ganti Baru\r\n'),
(3, 2, 1, 'Jumlah Barang', '2019-11-14', 'mobilnya pada dicuri', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `konvensasi`
--

CREATE TABLE `konvensasi` (
  `id` int(11) NOT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `periode` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konvensasi`
--

INSERT INTO `konvensasi` (`id`, `id_customer`, `periode`) VALUES
(1, 1, 'Januari - Maret');

-- --------------------------------------------------------

--
-- Table structure for table `kriteria_keluhan`
--

CREATE TABLE `kriteria_keluhan` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kriteria_keluhan`
--

INSERT INTO `kriteria_keluhan` (`id`, `nama`) VALUES
(0, 'Kategori Keluhan'),
(1, 'Jenis Penanganan'),
(2, 'Kelas Transaksi');

-- --------------------------------------------------------

--
-- Table structure for table `penanganan`
--

CREATE TABLE `penanganan` (
  `id` int(11) NOT NULL,
  `id_keluhan` int(11) DEFAULT NULL,
  `penanganan` varchar(100) DEFAULT NULL,
  `prioritas` int(11) DEFAULT NULL,
  `id_customer` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penanganan`
--

INSERT INTO `penanganan` (`id`, `id_keluhan`, `penanganan`, `prioritas`, `id_customer`) VALUES
(1, 1, 'Ganti Baru', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `kode_pengguna` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jabatan` varchar(15) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`kode_pengguna`, `nama`, `jabatan`, `foto`, `email`, `username`, `password`) VALUES
(1, 'akunting', 'accounting', NULL, 'a@a.c', 'akunting', '21232F297A57A5A743894A0E4A801FC3'),
(2, 'abdul', 'customer', NULL, 'c@c.c', 'customer', '21232F297A57A5A743894A0E4A801FC3'),
(3, 'humas', 'humas', NULL, 'h@h.h', 'humas', '21232F297A57A5A743894A0E4A801FC3'),
(4, 'direktur', 'direktur', NULL, 'd@d.d', 'direktur', '21232F297A57A5A743894A0E4A801FC3');

-- --------------------------------------------------------

--
-- Table structure for table `sub_kriteria_keluhan`
--

CREATE TABLE `sub_kriteria_keluhan` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `id_kriteria_keluhan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_kriteria_keluhan`
--

INSERT INTO `sub_kriteria_keluhan` (`id`, `nama`, `id_kriteria_keluhan`) VALUES
(1, 'a', 1),
(2, 'b', 1),
(3, 'c', 1),
(4, 'x', 2),
(5, 'y', 2);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `tujuan` varchar(50) DEFAULT NULL,
  `kelas` char(1) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `id_customer`, `tanggal`, `tujuan`, `kelas`, `qty`) VALUES
(1, 1, '2019-11-10', 'Bekasi', 'A', 12),
(2, 1, '2019-11-13', 'Bogor', 'C', 13);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keluhan`
--
ALTER TABLE `keluhan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `konvensasi`
--
ALTER TABLE `konvensasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kriteria_keluhan`
--
ALTER TABLE `kriteria_keluhan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penanganan`
--
ALTER TABLE `penanganan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`kode_pengguna`);

--
-- Indexes for table `sub_kriteria_keluhan`
--
ALTER TABLE `sub_kriteria_keluhan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `kode_pengguna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
